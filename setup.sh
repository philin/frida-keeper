#!/usr/bin/env bash
set -e

cd "$(dirname "$0")";
echo "Update npm";
npm install npm -g;

echo "Node version:" $(node --version);
echo "npm version: " $(npm --version);
echo "frida version:" $(frida --version);

npm install;

echo -e "\e[1;32m[+] Everything installed.\e[0m";
echo -e "\e[1;32m[+] Try to build a script";
npm run build frida-encryption;

if [[ ! -f "./_agent.js" ]]; then
    echo -e "\e[1;31m[!] Build process went wrong :(\e[0m";
    exit 1;
fi

echo -e "\e[1;32m[+] Try to spawn keeper and attach the compiled script (only works if Genymotion is running)\e[0m";
npm run spawn;

