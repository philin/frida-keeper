import Helper from  "./lib/helper"
import CLS from "./lib/classes"
import Formatter from "./lib/formatter"
import Log from "./lib/log"
import Colors from "./lib/colors"
import "./lib/ssl-pin-bypass"

Java.perform(function() {
    var complete_bytes: {[key: string] : string} = {}

    Helper.hookAllFunctionsInClass(CLS.org_spongycastle_crypto_PBEParametersGenerator)

    Helper.hookFunction(CLS.javax_crypto_spec_SecretKeySpec, "$init", function(_className, funcName, _parameters, args){
        this[funcName].apply(this, args)
        Log.color(Colors.BLUE, this)
        console.log("Creating " + args[1] + " secret key, plaintext:\n" + Formatter.hexdump(args[0]));
    })

    Helper.hookFunction(CLS.javax_crypto_spec_SecretKeySpec, "getEncoded")
    Helper.hookAllFunctionsInClass(CLS.java_security_spec_RSAPublicKeySpec)


    function cryptoHook(this: Java.Wrapper, _className: any, funcName: string, _parameters: any, args: any[]) {
        var ret = this[funcName].apply(this, args)
        Log.color(Colors.CYAN, this)

        var bytes = Formatter.bytesToHex(args[0])
        if (String(this) in complete_bytes) {
            complete_bytes[String(this)] += bytes
        }
        else {
            complete_bytes[String(this)] = bytes
        }

        console.log(info(this.getIV(), this.getAlgorithm(), String(this), ret));
        return ret;
    }

    Helper.hookFunction(CLS.javax_crypto_Cipher, "doFinal", cryptoHook)
    Helper.hookFunction(CLS.javax_crypto_Cipher, "update", cryptoHook)

    function info(iv: string | any[], alg: string, sThis: string, encoded: string | any[]) {
        var lines = []
        try {
            lines.push("Performing encryption/decryption");
            if (iv) {
                lines.push("Initialization Vector: \n" + Formatter.hexdump(iv));
            } else {
                lines.push("Initialization Vector: \n" + iv);
            }
            lines.push("Algorithm: " + Colors.PURPLE + alg + Colors.ENDC);
            lines.push("In: \n" + Formatter.hexdump(complete_bytes[sThis]));
            lines.push("Out: \n" + Formatter.hexdump(encoded));
        }
        catch(e){
            console.log("")
        }
        complete_bytes[sThis] = ""
        return lines.join("\n")
    }
});

Log.timestamp()