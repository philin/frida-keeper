
import helper from "./lib/helper"
import log from "./lib/log"
import './lib/ssl-pin-bypass'

Java.perform(async function(){

    var classes = await helper.searchForClasses("http", "url")
    for (var cls of classes) {
        helper.hookAllFunctionsInClass(cls)
    }
})

log.timestamp()