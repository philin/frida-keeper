import helper, { FilterFunc, ImplFunc } from "./lib/helper"
import log from "./lib/log"
import CLS from "./lib/classes"
import formatter from "./lib/formatter"

Java.performNow(function(){
    var filter: FilterFunc = function(methods) {
        var methodPattern = ["insert"]
        var res = []
        for (var method of methods) {
            for (var pattern of methodPattern) {
                if (method.toLowerCase().indexOf(pattern) > -1) {
                    res.push(method)
                }
            }
        }
        return res        
    }

    var impl: ImplFunc = function(className, funcName, parameters, args) {
        var ret = this[funcName].apply(this, args)
        console.log('Invoke ' + className + "." + funcName)
        if (args.length == 0) {
            return ret
        }

        var input = []
        for (var i in parameters) {
            var paraCls = parameters[i]
            var value = formatter.resolveObject(paraCls, args[i])
            input.push(`(${paraCls}) ${value}`)
        }
        var log = "\t[" + input.join(', ') + "]"
        console.log("\t" + log)
        if (args[0] == "password") {
            helper.printStacktrace()
        }
        return ret
    }

    var targets = [
        Java.use("android.database.sqlite.SQLiteDatabase"), 
        Java.use(CLS.io_requery_android_database_sqlite_SQLiteDatabase)
    ]

    for (var target of targets) {
        helper.hookAllFunctionsInClass(target, filter, impl)
    }
})

log.timestamp()

/*
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
                [(java.lang.String) properties, (java.lang.String) null, (android.content.ContentValues) app_uid=0 hits_count=39 adid=0 params=av=14.3.5.5&an=Keeper&aid=com.callpod.android_apps.keeper&aiid=com.aefyr.sai tid=UA-1797898-9 cid=d9812fac-b29f-4114-a30d-215ab48a8dd6, (int) 5]
Invoke android.database.sqlite.SQLiteDatabase.insertOrThrow
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
                [(java.lang.String) messages, (java.lang.String) null, (android.content.ContentValues) entry=[B@212a6a5 type=0, (int) 0]
Invoke android.database.sqlite.SQLiteDatabase.insertOrThrow
                [(java.lang.String) messages, (java.lang.String) null, (android.content.ContentValues) entry=[B@212a6a5 type=0]
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
                [(java.lang.String) properties, (java.lang.String) null, (android.content.ContentValues) app_uid=0 hits_count=40 adid=1 params=av=14.3.5.5&an=Keeper&aid=com.callpod.android_apps.keeper&aiid=com.aefyr.sai tid=UA-1797898-9 cid=d9812fac-b29f-4114-a30d-215ab48a8dd6, (int) 5]
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
                [(java.lang.String) properties, (java.lang.String) null, (android.content.ContentValues) app_uid=0 hits_count=41 adid=1 params=av=14.3.5.5&an=Keeper&aid=com.callpod.android_apps.keeper&aiid=com.aefyr.sai tid=UA-1797898-9 cid=d9812fac-b29f-4114-a30d-215ab48a8dd6, (int) 5]
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
                [(java.lang.String) properties, (java.lang.String) null, (android.content.ContentValues) app_uid=0 hits_count=42 adid=1 params=av=14.3.5.5&an=Keeper&aid=com.callpod.android_apps.keeper&aiid=com.aefyr.sai tid=UA-1797898-9 cid=d9812fac-b29f-4114-a30d-215ab48a8dd6, (int) 5]
Invoke android.database.sqlite.SQLiteDatabase.insertOrThrow
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
                [(java.lang.String) messages, (java.lang.String) null, (android.content.ContentValues) entry=[B@38cbaee type=0, (int) 0]
Invoke android.database.sqlite.SQLiteDatabase.insertOrThrow
                [(java.lang.String) messages, (java.lang.String) null, (android.content.ContentValues) entry=[B@38cbaee type=0]
Invoke io.requery.android.database.sqlite.SQLiteDatabase.insert
Invoke io.requery.android.database.sqlite.SQLiteDatabase.insertWithOnConflict
Invoke io.requery.android.database.sqlite.SQLiteDatabase.insertWithOnConflict
                [(java.lang.String) password, (java.lang.String) null, (android.content.ContentValues) shareable=1 udata=[B@9c77d95 shared=0 notes=[B@8720eaa ordering=0 client_modified_time=1.571058426754E12 latitude=0.0 watch_favorite=0 non_shared_data=[B@a843a9b link=[B@30a5e38 title=[B@b6f5e11 isModified=1 longitude=0.0 cipher=3 owner=1 record_key=[B@f3a6b76 editable=1 custom=[B@be1e577 secret2=[B@42325e4 version=2 revision=0 secret1=[B@adaca4d record_uid=QEgMJnVbLRCyvx5eEEXPPg deleted=0 encrypted=1 hasMeta=1 breach_watch_security_data=[B@d59c902 record_key_type=1 favorite=false, (int) 0]
 @ io.requery.android.database.sqlite.SQLiteDatabase.insert(SourceFile:1)
         Full call stack:dalvik.system.VMStack.getThreadStackTrace(Native Method)
        java.lang.Thread.getStackTrace(Thread.java:1538)
        io.requery.android.database.sqlite.SQLiteDatabase.insertWithOnConflict(Native Method)
        io.requery.android.database.sqlite.SQLiteDatabase.insert(SourceFile:1)
        io.requery.android.database.sqlite.SQLiteDatabase.insert(Native Method)
        _aa.a(SourceFile:170)
        _aa.a(SourceFile:109)
        _aa.a(SourceFile:108)
        com.callpod.android_apps.keeper.RecordEditFragment.k(SourceFile:26)
        com.callpod.android_apps.keeper.RecordEditFragment.save(SourceFile:1)
        com.callpod.android_apps.keeper.RecordEditFragment.Ba(SourceFile:2)
        com.callpod.android_apps.keeper.RecordEditFragment.onOptionsItemSelected(SourceFile:3)
        androidx.fragment.app.Fragment.performOptionsItemSelected(SourceFile:3)
        bi.b(SourceFile:103)
        Th.b(SourceFile:4)
        androidx.fragment.app.FragmentActivity.onMenuItemSelected(SourceFile:3)
        androidx.appcompat.app.AppCompatActivity.onMenuItemSelected(SourceFile:1)
        Ra.onMenuItemSelected(SourceFile:1)
        Ra.onMenuItemSelected(SourceFile:1)
        ra.onMenuItemClick(SourceFile:1)
        Gc.onMenuItemClick(SourceFile:2)
        androidx.appcompat.widget.ActionMenuView$c.a(SourceFile:2)
        cb.a(SourceFile:43)
        gb.g(SourceFile:2)
        cb.a(SourceFile:79)
        cb.a(SourceFile:76)
        androidx.appcompat.widget.ActionMenuView.a(SourceFile:1)
        androidx.appcompat.view.menu.ActionMenuItemView.onClick(SourceFile:2)
        android.view.View.performClick(View.java:6597)
        android.view.View.performClickInternal(View.java:6574)
        android.view.View.access$3100(View.java:778)
        android.view.View$PerformClick.run(View.java:25885)
        android.os.Handler.handleCallback(Handler.java:873)
        android.os.Handler.dispatchMessage(Handler.java:99)
        android.os.Looper.loop(Looper.java:193)
        android.app.ActivityThread.main(ActivityThread.java:6680)
        java.lang.reflect.Method.invoke(Native Method)
        com.android.internal.os.RuntimeInit$MethodAndArgsCaller.run(RuntimeInit.java:493)
        com.android.internal.os.ZygoteInit.main(ZygoteInit.java:858)

Invoke io.requery.android.database.sqlite.SQLiteDatabase.insert
                [(java.lang.String) password, (java.lang.String) null, (android.content.ContentValues) shareable=1 udata=[B@9c77d95 shared=0 notes=[B@8720eaa ordering=0 client_modified_time=1.571058426754E12 latitude=0.0 watch_favorite=0 non_shared_data=[B@a843a9b link=[B@30a5e38 title=[B@b6f5e11 isModified=1 longitude=0.0 cipher=3 owner=1 record_key=[B@f3a6b76 editable=1 custom=[B@be1e577 secret2=[B@42325e4 version=2 revision=0 secret1=[B@adaca4d record_uid=QEgMJnVbLRCyvx5eEEXPPg deleted=0 encrypted=1 hasMeta=1 breach_watch_security_data=[B@d59c902 record_key_type=1 favorite=false]
 @ _aa.a(SourceFile:170)
         Full call stack:dalvik.system.VMStack.getThreadStackTrace(Native Method)
        java.lang.Thread.getStackTrace(Thread.java:1538)
        io.requery.android.database.sqlite.SQLiteDatabase.insert(Native Method)
        _aa.a(SourceFile:170)
        _aa.a(SourceFile:109)
        _aa.a(SourceFile:108)
        com.callpod.android_apps.keeper.RecordEditFragment.k(SourceFile:26)
        com.callpod.android_apps.keeper.RecordEditFragment.save(SourceFile:1)
        com.callpod.android_apps.keeper.RecordEditFragment.Ba(SourceFile:2)
        com.callpod.android_apps.keeper.RecordEditFragment.onOptionsItemSelected(SourceFile:3)
        androidx.fragment.app.Fragment.performOptionsItemSelected(SourceFile:3)
        bi.b(SourceFile:103)
        Th.b(SourceFile:4)
        androidx.fragment.app.FragmentActivity.onMenuItemSelected(SourceFile:3)
        androidx.appcompat.app.AppCompatActivity.onMenuItemSelected(SourceFile:1)
        Ra.onMenuItemSelected(SourceFile:1)
        Ra.onMenuItemSelected(SourceFile:1)
        ra.onMenuItemClick(SourceFile:1)
        Gc.onMenuItemClick(SourceFile:2)
        androidx.appcompat.widget.ActionMenuView$c.a(SourceFile:2)
        cb.a(SourceFile:43)
        gb.g(SourceFile:2)
        cb.a(SourceFile:79)
        cb.a(SourceFile:76)
        androidx.appcompat.widget.ActionMenuView.a(SourceFile:1)
        androidx.appcompat.view.menu.ActionMenuItemView.onClick(SourceFile:2)
        android.view.View.performClick(View.java:6597)
        android.view.View.performClickInternal(View.java:6574)
        android.view.View.access$3100(View.java:778)
        android.view.View$PerformClick.run(View.java:25885)
        android.os.Handler.handleCallback(Handler.java:873)
        android.os.Handler.dispatchMessage(Handler.java:99)
        android.os.Looper.loop(Looper.java:193)
        android.app.ActivityThread.main(ActivityThread.java:6680)
        java.lang.reflect.Method.invoke(Native Method)
        com.android.internal.os.RuntimeInit$MethodAndArgsCaller.run(RuntimeInit.java:493)
        com.android.internal.os.ZygoteInit.main(ZygoteInit.java:858)

Invoke io.requery.android.database.sqlite.SQLiteDatabase.insert
Invoke io.requery.android.database.sqlite.SQLiteDatabase.insertWithOnConflict
Invoke io.requery.android.database.sqlite.SQLiteDatabase.insertWithOnConflict
                [(java.lang.String) user_folder_record, (java.lang.String) null, (android.content.ContentValues) record_uid=QEgMJnVbLRCyvx5eEEXPPg folder_uid=root revision=0, (int) 0]
Invoke io.requery.android.database.sqlite.SQLiteDatabase.insert
                [(java.lang.String) user_folder_record, (java.lang.String) null, (android.content.ContentValues) record_uid=QEgMJnVbLRCyvx5eEEXPPg folder_uid=root revision=0]
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
                [(java.lang.String) properties, (java.lang.String) null, (android.content.ContentValues) app_uid=0 hits_count=43 adid=1 params=av=14.3.5.5&an=Keeper&aid=com.callpod.android_apps.keeper&aiid=com.aefyr.sai tid=UA-1797898-9 cid=d9812fac-b29f-4114-a30d-215ab48a8dd6, (int) 5]
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
                [(java.lang.String) properties, (java.lang.String) null, (android.content.ContentValues) app_uid=0 hits_count=44 adid=1 params=av=14.3.5.5&an=Keeper&aid=com.callpod.android_apps.keeper&aiid=com.aefyr.sai tid=UA-1797898-9 cid=d9812fac-b29f-4114-a30d-215ab48a8dd6, (int) 5]
[Android Device::com.callpod.android_apps.keeper]-> Invoke android.database.sqlite.SQLiteDatabase.insertOrThrow
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
                [(java.lang.String) messages, (java.lang.String) null, (android.content.ContentValues) entry=[B@278f2f type=0, (int) 0]
Invoke android.database.sqlite.SQLiteDatabase.insertOrThrow
                [(java.lang.String) messages, (java.lang.String) null, (android.content.ContentValues) entry=[B@278f2f type=0]
Invoke android.database.sqlite.SQLiteDatabase.insertOrThrow
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict                                                                                                                 
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
                [(java.lang.String) messages, (java.lang.String) null, (android.content.ContentValues) entry=[B@97b8db type=0, (int) 0]
Invoke android.database.sqlite.SQLiteDatabase.insertOrThrow
                [(java.lang.String) messages, (java.lang.String) null, (android.content.ContentValues) entry=[B@97b8db type=0]
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
                [(java.lang.String) properties, (java.lang.String) null, (android.content.ContentValues) app_uid=0 hits_count=45 adid=1 params=av=14.3.5.5&an=Keeper&aid=com.callpod.android_apps.keeper&aiid=com.aefyr.sai tid=UA-1797898-9 cid=d9812fac-b29f-4114-a30d-215ab48a8dd6, (int) 5]
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
Invoke android.database.sqlite.SQLiteDatabase.insertWithOnConflict
                [(java.lang.String) properties, (java.lang.String) null, (android.content.ContentValues) app_uid=0 hits_count=46 adid=1 params=av=14.3.5.5&an=Keeper&aid=com.callpod.android_apps.keeper&aiid=com.aefyr.sai tid=UA-1797898-9 cid=d9812fac-b29f-4114-a30d-215ab48a8dd6, (int) 5]
Server terminated
[Android Device::com.callpod.android_apps.keeper]->    

*/
