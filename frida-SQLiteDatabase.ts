import helper, { FilterFunc } from "./lib/helper"
import log from "./lib/log"
import CLS from "./lib/classes"

Java.performNow(function(){
    var filter: FilterFunc = function(methods) {
        var methodPattern = ["delete", "insert", "update", "execSQL"]
        var res = []
        for (var method of methods) {
            for (var pattern of methodPattern) {
                if (method.toLowerCase().indexOf(pattern) > -1) {
                    res.push(method)
                }
            }
        }
        return res
    }

    var targets = [
        Java.use("android.database.sqlite.SQLiteDatabase"), 
        Java.use(CLS.io_requery_android_database_sqlite_SQLiteDatabase)
    ]

    for (var target of targets) {
        helper.hookAllFunctionsInClass(target, filter)
    }
})

log.timestamp()