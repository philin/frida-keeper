
# Requirements
* NodeJS
* npm

# Usage
1. Install dependencies
    ```
    npm install
    ```
2. Inside the root folder of the repository are scripts starting with `frida-` these are intended to run during a frida session (only one at a time). To do so, a module has to be built first. 
    ```
    npm run build [name of the desired module]
    ```

3. To run the module in a Frida session, run `npm run spawn`. This basically executes the following in the background, whereas `_agent.js` is the built module from step 2. That means it spawns Keeper and runs the script.
    ```
    frida -U -f com.callpod.android_apps.keeper --runtime=v8 -l _agent.js --no-pause
    ```

4. Inside the FridaCLI two new objects are available.
    * `helper` provides access to all functions defined in `lib/helper.ts`
    * `classes` provides access for interesting Java classes (`/lib/classes.ts`), feel free to add more if you find interesing ones.
    * `colors` provides acces to the colors for printing logging in different colors
    * `log` provides functions to log the current time, a seperator, or in a different color
    * `formatter` includes functions for converting bytes hex strings and reverse as well as a function to create a fancy hexdump

## Why is the fucking build process needed (step 2)?
Frida does not support ES6 modules or CommonJS (require()) stuff. Therefore a npm module `frida-compile` exists that basically runs browserify in the background to provide support for modules in Frida scripts.

## Available npm run scripts
* `npm run build [moduleName]`, builds compiles a module into `_agent.js`. `[moduleName]` is for example `frida-encryption.ts`.
* `npm run watch [moduleName]`, same as `build` but does not terminate, it monitors the files and if one file changes starts the build process again immediately. Perfect for development.
* `npm run spawn`, spawns a new process of Keeper and runs `_agent.js` inside the Frida CLI.
* `npm run attach`, same as `spawn` but attaches to a currently running Keeper process.
* `npm run restart-frida`, restarts frida-server on the phone, in case anything goes wrong.
* `npm run start-frida`, starts the frida-server.


## Attention
Everything that interacts with functions out of the Java world has to be wrapped into a `Java.perform()` block.
```javascript
Java.perform(() =>{
    // do stuff here, e.g. searching for classes,
    // hooking functions etc.
})
```

# Standalone scripts
* Includes standalone scripts for Frida (use it on your own risk)
* With `searchForHexInFiles.py` it is possible to search in all files inside a folder for bytes provided as hex string
* `decrypt_vault.py` trys to decrypt a Keeper vault, given the master password and the database.
