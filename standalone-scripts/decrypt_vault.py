#!/usr/bin/env python3

# requires pycryptodome!!
import hashlib, argparse, sqlite3, os, sys
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad

args = None

def pbkdf2(salt):
    return hashlib.pbkdf2_hmac("sha256", args.masterPassword, salt, 100000)

def decryptAES(key, iv, cipherText):
    # print("\n")
    # print("key: {}".format(key.hex()))
    # print("iv:  {}".format(iv.hex()))
    # print("ct:  {}".format(cipherText.hex()))
    cipher = AES.new(key, AES.MODE_CBC, iv)
    pt = cipher.decrypt(cipherText)
    #print("pt: {}".format(pt.hex()))
    return pt

'''
* Salt to derive k1
    * In database, setting -> name=encryption_params[4:20]
* Ciphertext to decrypt k2
    * In database, setting -> name=encryption_params[20:100]
* Salt to derive k3 
    * In database, setting -> name=client_key[3:19]
* Ciphertext to decrypt d
    * In database, setting -> name=client_key[19:99]
'''

def main():
    global args
    parser = argparse.ArgumentParser("This tool calculates the vault encryption key, given a master password and the SQLiteDatabase from the Android phone.")
    parser.add_argument("masterPassword", help="Master Password (normal string) to unlock Keeper")
    parser.add_argument("SQLiteDatabase", help="Path to the SQLiteDatabase")

    args = parser.parse_args()

    args.masterPassword = args.masterPassword.encode("utf-8")

    if not os.path.exists(args.SQLiteDatabase):
        print("File '{}' does not exists.".format(args.SQLiteDatabase))
        sys.exit(1)

    conn = sqlite3.connect(args.SQLiteDatabase)
    cur = conn.cursor()
    cur.execute("select setting_str from setting where name='encryption_params'")
    encryption_params = cur.fetchone()[0]

    cur.execute("select setting_str from setting where name='client_key'")
    client_key = cur.fetchone()[0]

    salt_k1 = encryption_params[4:20]
    print("Salt for k1: {}".format(salt_k1.hex()))

    cipherText_to_decrypt_k2 = encryption_params[20:100]
    print("Ciphertext to decrypt k2: {}".format(cipherText_to_decrypt_k2.hex()))

    salt_k3 = client_key[3:19]
    print("Salt for k3: {}".format(salt_k3.hex()))

    cipherText_to_decrypt_d = client_key[19:99]
    print("Ciphertext to decrypt d: {}".format
    (cipherText_to_decrypt_d.hex()))

    k1 = pbkdf2(salt_k1)
    k2 = decryptAES(k1, cipherText_to_decrypt_k2[:16], cipherText_to_decrypt_k2[16:])[:32]
    k3 = pbkdf2(salt_k3)
    d = decryptAES(k3, cipherText_to_decrypt_d[:16], cipherText_to_decrypt_d[16:])
    k4 = unpad(decryptAES(k2, d[:16], d[16:]), AES.block_size)

    print("\nK1: {}".format(k1.hex()))
    print("K2: {}".format(k2.hex()))
    print("K3: {}".format(k3.hex()))
    print(" d: {}".format(d.hex()))
    print("K4: {}".format(k4.hex()))

    vault_key = k4

    print("\n\nDecrypt database:")
    for pw in cur.execute("select title,secret1,secret2,notes,link,extra,udata,non_shared_data,breach_watch_security_data from password"):
        fields = ["title","secret1","secret2","notes","link","extra","udata","non_shared_data","breach_watch_security_data"]
        for i in range(len(pw)):
            encrypted = pw[i]
            field = fields[i]
            try:
                decrypted = unpad(decryptAES(vault_key, encrypted[:16], encrypted[16:]), AES.block_size)
            except:
                continue

            print("{:7s}: {}".format(field, decrypted))

        print("")


if __name__ == "__main__":
    main()
