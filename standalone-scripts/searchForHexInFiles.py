#!/usr/bin/env python3
import os, sys
import argparse, re, binascii, math

args = None

def hexdump(hexstring, highlightStart=0, highlightEnd=0, blockSize=4, lineSize=16, lineNumbers=True, lineNumbersDecimal=False, lineNumberOffset=0, highlight=True, linePrefix=''):
    lines = []
    groupsPerLine = lineSize // blockSize
    charsPerBlock = blockSize * 2
    split = hexstring
    byteGroups = []
    while split != "":
        byteGroups.append(split[:charsPerBlock])
        if len(split) > charsPerBlock:
            split = split[charsPerBlock:]
        else:
            split = ""

    linesLeft = []
    index = 0
    while len(byteGroups) != 0:
        if index % groupsPerLine == 0:
            linesLeft.append("")
        linesLeft[-1] += byteGroups.pop(0) + " "
        index += 1

    highlight = False if highlightEnd == highlightStart else highlight

    if highlight:
        # do highlighting
        hStart  = highlightStart // (groupsPerLine * charsPerBlock)
        hStartI = highlightStart % (groupsPerLine * charsPerBlock)
        hEnd  = highlightEnd // (groupsPerLine * charsPerBlock)
        hEndI = highlightEnd % (groupsPerLine * charsPerBlock)
        start_leftLine = hStartI + hStartI // charsPerBlock
        end_leftLine = hEndI + hEndI // charsPerBlock
        start_rightLine = hStartI // 2
        end_rightLine = hEndI // 2

    for line in linesLeft:
        lineRight = hexToString(line.replace(" ", ""))
        c = (charsPerBlock * groupsPerLine + groupsPerLine) - len(line)
        c = max(0, c)
        if len(linesLeft) == 1:
            c = 0
        spacer = " " * c

        if highlight:
            index = linesLeft.index(line)
            if index == hStart and index == hEnd:
                line = line[:start_leftLine] + "\033[1;31m" + line[start_leftLine:end_leftLine] + "\033[0m" + line[end_leftLine:]
                lineRight = lineRight[:start_rightLine] + "\033[1;31m" + lineRight[start_rightLine:end_rightLine] + "\033[0m" + lineRight[end_rightLine:]
            elif index == hStart:
                line = line[:start_leftLine] + "\033[1;31m" + line[start_leftLine:] + "\033[0m"
                lineRight = lineRight[:start_rightLine] + "\033[1;31m" + lineRight[start_rightLine:] + "\033[0m"
            elif index > hStart and index < hEnd:
                line = "\033[1;31m" + line + "\033[0m"
                lineRight = "\033[1;31m" + lineRight + "\033[0m"
            elif index == hEnd:
                line = "\033[1;31m" + line[:end_leftLine] + "\033[0m" + line[end_leftLine:]
                lineRight = "\033[1;31m" + lineRight[:end_rightLine] + "\033[0m" + lineRight[end_rightLine:]

        lines.append(line + spacer + "  " + lineRight)
    
    if lineNumbers:
        if not lineNumbersDecimal:
            requiredNumbers = math.ceil(math.log(len(lines) * groupsPerLine * blockSize, 16))
            fmtStr = "%0" + str(requiredNumbers + 2) + "x"
            lines = list(map(lambda x:  "{}: {}".format(fmtStr % (x * lineSize + lineNumberOffset), lines[x]), range(len(lines))))
        else:
            requiredNumbers = math.ceil(math.log(len(lines) * groupsPerLine * blockSize, 10))
            fmtStr = "%0" + str(requiredNumbers + 2) + "d"
            lines = list(map(lambda x: "{}: {}".format(fmtStr % (x * lineSize + lineNumberOffset), lines[x]), range(len(lines))))

    lines = list(map(lambda x: linePrefix + x, lines))

    return "\n".join(lines)

def hexToString(hexx):
    res = ""
    while len(hexx) != 0:
        b = bytes.fromhex(hexx[:2])
        hexx = hexx[2:]
        i = int.from_bytes(b, 'big')
        c = "."
        if i > 32 and i < 127:
            c = b.decode('latin1')
        res += c
    return res

def main():
    global args
    parser = argparse.ArgumentParser(description="Searches in all files inside a folder for a given hex string")
    parser.add_argument("folder", help="Folder to search in")
    parser.add_argument("pattern", help="Hex string (bytes) to search for")
    parser.add_argument("-s", "--string", help="Pattern should be interpreted as string", action='store_true')
    parser.add_argument("-b", "--before", help="Print [before] bytes before the pattern", type=int, default=0)
    parser.add_argument("-a", "--after", help="Print [after] bytes after the pattern", type=int, default=0)

    #args = parser.parse_args(["/Users/philipp/Desktop/Forensics/FromAndroidDevice/data4", "-b", "200", "85a26c95316bb02785aa5f98f0de0a3ed2e9f231f2a14548f93597280fbf0ac0b81a311aca3950fd0ea55012f21df119d80a839183944e72b95642e5f1ce43328884d131eb9f811515d2d30bd2e9a1a8"])
    args = parser.parse_args()

    if args.string:
        args.pattern = args.pattern.encode("utf-8").hex()

    args.patternBytes = bytes.fromhex(args.pattern)
    args.before *= 2
    args.after *= 2

    for (dirpath, dirnames, filenames) in os.walk(args.folder):
        files = list(map(lambda x: os.path.join(dirpath, x), filenames))

        for f in files:
            with open(f, "rb") as fi:
                content = fi.read()

            matches = list(re.finditer(args.pattern, content.hex().lower()))
            if len(matches) > 0:
                print(f)
            for match in matches:
                start = match.start(0)
                end = match.end(0)
                dump = hexdump(content.hex().lower()[start - args.before:end + args.after], args.before, args.before + (end - start), lineNumbers=True, linePrefix='    ', lineSize=32)
                
                print(dump)



if __name__ == "__main__":
    main()