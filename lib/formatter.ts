import CLS from "./classes"

class Formatter {

    /**
     * Formats a memory pointer to a Java object into a readable string for debugging
     *
     * @param {string} className Java class name
     * @param {any} value memory pointer (e.g. function argument) that an object of type `className`
     * @returns string for debugging
     */
    resolveObject(className: string, value: any) {
        switch (className) {
            case CLS.java_net_URL:
                var i = Java.cast(value, Java.use(CLS.java_net_URL))
                return i.toString()
            case CLS.bytearray:
                return this.bytesToHex(value)
            default:
                return value
        }
    }

    /**
     * Generates a hexdump string
     *
     * @param {string | any[]} buffer Hexstring or byte array to dump
     * @param {int} blockSize optional, how many bytes are printed in one block. Default is 4.
     * @returns hexdump string
     */
    hexdump(buffer: string | any[], blockSize?: number): string {
        var lines = []
        
        // Don't use Array.isArray, that does not work
        // Arrays can be Java Wrapper Objects, that are different from JS Arrays, but can be handled as such...
        if (typeof buffer === 'object') {
            buffer = this.bytesToHex(buffer)
        }

        // strip 0 bytes
        buffer = buffer.replace(/(00)+$/g, "")

        if (!blockSize) blockSize = 4
        blockSize = blockSize * 2
        // splits a string in substrings of max. length of blocksize * 2
        var byteGroups = buffer.match(new RegExp(".{1," + blockSize.toString() + "}", "g"))
        var lines_left = []; var index = 0;
        while (byteGroups && byteGroups.length != 0) {
            if (index % 4 == 0) {
                lines_left.push("")
            }
            lines_left[lines_left.length-1] += byteGroups.shift() + " "
            index += 1
        }
        
        for (var line of lines_left) {
            var line_right = this.hexToString(line.replace(/\s/g, ""))
            var c = (blockSize * 4 + 4) - line.length
            if (c < 0) c = 0;
            var spacer = " ".repeat(c)
            lines.push(line + spacer + "\t" + line_right)
        }

        lines = lines.map(function(x){ return "\t" + x })
        lines.splice(0, 0, buffer)
        return lines.join("\n");
    }

    /**
     * Takes a hex string and transforms it to a ascii string
     *
     * @param {string} hexx String of hex characters, basically [0-9a-fA-F]+
     * @returns Ascii string
     */
    hexToString(hexx: string): string {
        var hex = hexx.toString();
        var str = '';
        for (var i = 0; i < hex.length; i += 2){
            var int = parseInt(hex.substr(i, 2), 16)
            var char = "."
            if (int >= 32 && int <= 126) {
                // only transform non weired characters
                char = String.fromCharCode(parseInt(hex.substr(i, 2), 16));
            }
            
            str += char
        }
           
        str.replace(/\n/g, "\\n")
        str.replace(/\t/g, "\\t")
        str.replace(/\r/g, ".")
        return str;
    }

    /**
     * Takes a bytes array and transforms it into a hex string
     *
     * @param {Array} array Byte array
     * @returns Hex String
     */
    bytesToHex(array: any[]) : string {
        var result = ""
        for (var i = 0; i < array.length; i++) {
            result += modulus(array[i], 256).toString(16).padStart(2, '0');
        }
        return result;
    }
}
function modulus(x: number, n: number) {
    return ((x % n) + n) % n;
}

export default new Formatter()

// @ts-ignore
globalThis.formatter = new Formatter()