
enum Colors {
    PURPLE = '\x1B[0;95m',
    BLUE =  '\x1B[0;94m',
    CYAN = '\x1B[0;96m',
    GREEN = '\x1B[0;92m',
    GRAY = '\x1B[1;90m',
    ENDC = '\x1B[0m',
}
export default Colors

// @ts-ignore
globalThis.colors = Colors