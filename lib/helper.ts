import formatter from "./formatter"
import Classes from './classes'
import { isString } from 'util'
import Log from './log'
import Colors from './colors'

export interface FilterFunc {
    (methods: string[]) : string[]
}

export interface ImplFunc {
    (this: Java.Wrapper, className: string, funcName: string, parameters: string[], args: any[]) : any
}

class Helper {

    /**
     * Wraps a function into a Java.performNow() block.
     * It is not necassary to call this function outside of functions of the helper module
     *
     * @param {Function} func a function that should be executed
     * @returns Promise with the result of Function
     */
    executeInJavaVM(func: Function): Promise<any> {
        return new Promise((resolve) => {
            Java.performNow(() => {
                resolve(func())
            })
        })
    }

    /**
     * Hooks all functions in class, with the option to filter functions
     * and to provide a custom implementation.
     *
     * @param {Classes | Java.Wrapper} cls ClassName or Java class object of which the functions should be hooked.
     * @param {Function} filter Filter function to be able to hook not all functions. Gets an list of methods (strings) as argument and must return a subset of the list of methods again.
     * @param {Function} implementation Custom implementation of the hooked function, is passed to `hookFunction()`
     */
    hookAllFunctionsInClass(cls: Classes | Java.Wrapper, filter?: FilterFunc, implementation?: ImplFunc) : void {
        if (Java.vm.tryGetEnv() == null) {
            var func = this.hookAllFunctionsInClass
            // @ts-ignore
            this.executeInJavaVM(func.apply.bind(func, this, arguments))
            return
        }

        if (isString(cls)) {
            cls = Java.use(cls)
        }
        
        var methods = this.listFunctionsOfClass(cls)
        if (filter != null) {
            methods = filter(methods)
        }

        for (var method of methods) {
            this.hookFunction(<Classes>cls.$className, method, implementation)
        }
    }

    /**
     * Hooks a function (funcName) of a class (className)
     *
     * @param {string} className Name of the class the function belongs to
     * @param {string} funcName Name of the function
     * @param {Function} implementation Optional implementation (function) that is executed when the hooked function is called. The default implementation (if `implementation === null`) logs the arguments. The function gets the following arguments: className, funcName, parameters, original arguments to the hooked function. 
     * 
     * Inside the implementation function the original function should be called like `var ret = this[funcName].apply(this, args)`
     */
    hookFunction(className: Classes, funcName: string, implementation?: ImplFunc): void {
        if (funcName == "$new") return
        console.log("hook function: " + className + "." + funcName)
        if (Java.vm.tryGetEnv() == null) {
            var func = this.hookFunction
            // @ts-ignore
            this.executeInJavaVM(func.apply.bind(func, this, arguments))
            return
        }
        try {
            var clazz = Java.use(className)
            var overloads = clazz[funcName].overloads;
            for (var i in overloads) {
                if (!overloads[i].hasOwnProperty('argumentTypes')) {
                    continue
                }
    
                var parameters: string[] = [];
                var curArgumentTypes = overloads[i].argumentTypes;
                for (var j in curArgumentTypes) {
                    parameters.push(curArgumentTypes[j].className);
                }

                var returnType = overloads[i].returnType.className
                
                let impl : ImplFunc
                if (!implementation) {
                    impl = function(this: Java.Wrapper) {
                        var args = [].slice.call(arguments)
                        var ret: any;
                        if (returnType != "void") {
                            ret = this[funcName].apply(this, args)
                        }
                        else {
                            this[funcName].apply(this, args)
                        }
 
                        console.warn('Invoke ' + className + "." + funcName)
                        try {
                            Log.color(Colors.BLUE, "  " + this)
                        } catch (e) { }
                        
                        if (args.length == 0) {
                            return ret
                        }
        
                        var input = []
                        for (i in parameters) {
                            var paraCls = parameters[i]
                            var value = formatter.resolveObject(paraCls, args[i])
                            input.push(`(${paraCls}) ${value}`)
                        }
                        var log = "\t[" + input.join(', ') + "]"
                        console.log("\t" + log)
                        if (returnType != "void") {
                            return ret
                        }
                    }
                }
                else {
                    impl = function(this: Java.Wrapper) {
                        console.warn('Invoke ' + className + "." + funcName)
                        return implementation.call(this, className, funcName, parameters, [].slice.call(arguments))
                    }
                }
                overloads[i].implementation = impl
            }
        } catch (error) {
            console.error("failed:", className + '.' + funcName, '\n\t', error)
        }
    }

    /**
     * Prints a stack trace. It could be useful to call this from implementations passed to `hookFunction()`.
     *
     */
    printStacktrace() : void {
        if (Java.vm.tryGetEnv() == null) {
            var func = this.printStacktrace
            // @ts-ignore
            this.executeInJavaVM(func.apply.bind(func, this, arguments))
            return
        }
        var threadef = Java.use('java.lang.Thread')
        var threadinstance = threadef.$new()
        var stack = threadinstance.currentThread().getStackTrace()
        var full_call_stack = Where(stack)
        console.log(" @ " + stack[3].toString() + "\n\t Full call stack:" + full_call_stack) 
    }

    /**
     * Returns a list functions that belong to a class.
     *
     * @param {string | Java.Wrapper} cls Class
     * @returns string[] of strings containig all functions of cls
     */
    listFunctionsOfClass(cls: string | Java.Wrapper) : string[] {
        if (Java.vm.tryGetEnv() == null) {
            var func = this.listFunctionsOfClass
            // @ts-ignore
            return this.executeInJavaVM(func.apply.bind(func, this, arguments))
        }
        if (typeof cls == "string") {
            cls = Java.use(cls)
        }
        return Object.getOwnPropertyNames(cls.$classWrapper.prototype)
    }

    /**
     * Enumerates all loaded java classes and searches for pattern.
     * Returns a Promise that resolves to list of class names matching pattern.
     *
     * @param {string} pattern Pattern that is used to search for classes. Multiple arguments possible.
     * @returns {Promise<string[]>}
     */
    searchForClasses(...pattern: string[]) : Promise<Classes[]>{
        if (Java.vm.tryGetEnv() == null) {
            var func = this.searchForClasses
            // @ts-ignore
            return this.executeInJavaVM(func.apply.bind(func, this, arguments))
        }
        return new Promise((res) => {
            console.log("\n----------"+pattern+"----------")
            let classes: Classes[] = [];
            for (let i = 0; i < pattern.length; i++) {
                Java.enumerateLoadedClasses({
                    "onMatch": function(className){
                        if(className.toLowerCase().indexOf(pattern[i].toLowerCase()) != -1){
                            classes.push(<Classes>className)
                        }
                    },
                    "onComplete":function(){
                        if (i == (pattern.length - 1)) {
                            classes.sort()
                            console.log(classes.join("\n"))
                            res(classes)
                        }
                    }
                })
            }
        })
    }
}


function Where(stack: any){
    var at = ""
    for(var i = 0; i < stack.length; ++i){
        at += stack[i].toString() + "\n\t"
    }
    return at
}

export default new Helper()

// @ts-ignore
globalThis.helper = new Helper()