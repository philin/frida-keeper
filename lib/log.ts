import Colors from "./colors"

export default class Log {
    /**
     * Print a timestamp, useful in Frida CLI
     *
     */
    static timestamp(): void {
        var date = new Date( new Date().getTime() + 2 * 3600 * 1000).toUTCString().replace( / GMT$/, "" )
        console.error(date)
    }

    /**
     * Print a seperator, useful in Frida CLI
     *
     */
    static seperator(): void {
        Log.timestamp()
        for (var index = 0; index < 10; index++) {
            console.error("------------------------")
        }
    }

    /**
     * Log in a different color for a better overview
     *
     * @param {color} color Use a value from the colors module for this argument, e.g. colors.BLUE
     * @param {*} args Any amout of arguments, passed to console.log
     */
    static color(color: Colors, arg0?:any, ...args: any[]): void {
        args.push(Colors.ENDC)
        args.splice(0, 0, arg0)
        console.log.apply(null, [color, ...args])
    }
}

Log.timestamp()

// @ts-ignore
globalThis.log = Log