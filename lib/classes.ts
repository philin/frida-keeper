

enum Classes {
    // http, url, networing
    java_net_URL                     = "java.net.URL",
    java_net_HttpUrlConnection       = "java.net.HttpURLConnection",
    java_net_URLConnection           = "java.net.URLConnection",
    javax_net_ssl_HttpsURLConnection = "javax.net.ssl.HttpsURLConnection",
    com_android_okhttp               = "com.android.okhttp",

    // database
    io_requery_android_database_sqlite_SQLiteDatabase = "io.requery.android.database.sqlite.SQLiteDatabase",
    android_database_sqlite_SQLiteDatabase            = "android.database.sqlite.SQLiteDatabase",

    // crypto
    javax_crypto_spec_SecretKeySpec                = "javax.crypto.spec.SecretKeySpec",
    java_security_spec_RSAPublicKeySpec            = "java.security.spec.RSAPublicKeySpec",
    javax_crypto_Cipher                            = 'javax.crypto.Cipher',
    org_spongycastle_crypto_PBEParametersGenerator = "org.spongycastle.crypto.PBEParametersGenerator",

    // data types
    bytearray = "[B",
}

export default Classes

// @ts-ignore
globalThis.classes = Classes